FROM python:3.6
ADD . /code
WORKDIR /code
RUN pip install pymongo -i https://pypi.tuna.tsinghua.edu.cn/simple
