from config import *
import pymongo
import re
from collections import OrderedDict
import csv

client = pymongo.MongoClient(host=MONGO_HOST, port=MONGO_PORT, )

db_image = client[MONGO_DB_IMAGE]
coll_bo_image = db_image[MONGO_COLL_BOOKS_IMAGES]
coll_in_image = db_image[MONGO_COLL_INSP_IMAGES]
coll_pa_image = db_image[MONGO_COLL_PATTERNS_IMAGES]
coll_st_image = db_image[MONGO_COLL_STYLES_IMAGES]
coll_re_image = db_image[MONGO_COLL_REFER_IMAGES]
coll_tr_image = db_image[MONGO_COLL_TRENDS_IMAGES]
coll_an_image = db_image[MONGO_COLL_ANALYSIS_IMAGES]
coll_ru_image = db_image[MONGO_COLL_RUNWAYS_IMAGES]
st_image = coll_st_image.find()
num = 0
with open('款式.csv', 'w', newline='') as f:
    f_csv = csv.writer(f)
    f_csv.writerow(
        ['sex', 'industry', 'column', 'season', 'region', 'brand', 's_product', 'style', 'element', 'view_count',
         'updateTime', 'stitle', 'cover_img', 'title_img', 'label'])

for i in st_image:
    num += 1
    if i['col'] in [122, 55, 123]:
        item = OrderedDict()
        view_count = i['view_count']
        updateTime = i['updateTime']
        stitle = i['stitle']
        cover_img = i['detailList'][0]['smallPath']
        title_img = i['detailList'][0]['bigPath']
        sex = i['styleInfo'][0]['name'].strip()
        industry = i['styleInfo'][1]['name'].strip()
        column = None
        if i['col'] == 122:
            column = '设计师品牌'
        if i['col'] == 55:
            column = '名牌精选'
        if i['col'] == 123:
            column = '款式流行'
        res = re.findall('\d+', i['styleInfo'][2]['name'].strip())
        if res:
            season = i['styleInfo'][2]['name'].strip()
        else:
            season = None
        region_data = i['styleInfo'][3]['name'].strip()
        res1 = re.match('[\u4e00-\u9fa5]+$', region_data)
        if res1:
            region = region_data
        else:
            region = None
        if not region:
            brand = region_data

        else:
            brand = i['styleInfo'][4]['name'].strip()
        label = [k['name'] for k in i['styleInfo']]
        long_str = ''.join(label)
        res2 = None
        for i in product_list:
            res2 = re.search('{}'.format(i), long_str)
        if res2:
            break
        s_product = res2.group() if res2 else None
        res3 = None
        for i in style_list:
            res3 = re.search('{}'.format(i), long_str)
            if res3:
                break
        style = res3.group() if res3 else None
        res4 = None
        for i in element_list:
            res4 = re.search('{}'.format(i), long_str)
            if res4:
                break
        element = res4.group() if res4 else None
        item = {'sex': sex, 'industry': industry, 'column': column, 'season': season, 'region': region, 'brand': brand,
                's_product': s_product, 'style': style, 'element': element, 'view_count': view_count,
                'updateTime': updateTime, 'stitle': stitle, 'cover_img': cover_img, 'title_img': title_img,
                'label': label}
        print(item,num)
        with open('款式.csv','a+',newline='') as f:
            f_csv = csv.writer(f)
            f_csv.writerow(item.values())
